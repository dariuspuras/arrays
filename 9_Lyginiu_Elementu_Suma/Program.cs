﻿using System;

namespace _9_Lyginiu_Elementu_Suma
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 8 UZDUOTIS:
             * Suskaiciuoti elementu suma, kurie dalinasi is 2 
            */

            Console.WriteLine();
            Console.WriteLine("----- 8 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);
            int lyginiuElementuSuma = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                if (masyvas[i] % 2 == 0)
                {
                    lyginiuElementuSuma = lyginiuElementuSuma + masyvas[i];
                }
            }

            Console.WriteLine();
            Console.WriteLine("suma elementu kurie dalinasi is 2: " + lyginiuElementuSuma);
            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
