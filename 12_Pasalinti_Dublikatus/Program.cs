﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _12_Pasalinti_Dublikatus
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine();
            Console.WriteLine("----- DUBLIKATU PASALINIMAS -------");
            Console.WriteLine();

            int[] masyvas = new int[100];
            Random randNum = new Random();

            // Uzpildo masyva random reiksmemis
            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 10);
            }

            // Printina masyvo elementus i konsole
            Console.WriteLine("Pradinis masyvas: ");

            foreach (int elementas in masyvas)
            {
                Console.Write(elementas + " ");
            }

            Console.WriteLine();

            DateTime laikasPries = DateTime.Now;

            //int[] masyvas2 = new int[] { }; // Neapibrezto dydzio masyvo sukurimas
            List<int> sarasas = new List<int>(); // Susikurti tuscia list'a, kad galima butu naudoti Add()

            // Irasom i lista pirmaji masyvo elementa
            sarasas.Add(masyvas[0]);

            bool arDublikatas;

            foreach (int masElem in masyvas)
            {
                arDublikatas = false;

                foreach (int listElem in sarasas)
                {
                    // Tikrina ar elementas yra dublikatas
                    if (masElem == listElem)
                    {
                        arDublikatas = true;
                        break;
                    }
                }

                // Jei elementas ne dublikatas, tai iraso i sarasa
                if (arDublikatas == false)
                {
                    sarasas.Add(masElem);
                }                
            }

            // List tipa konvertuojam i array tipa
            sarasas.ToArray();

            DateTime laikasPo = DateTime.Now;

            // Printina masyva be dublikatu
            Console.WriteLine();
            Console.WriteLine("Masyvas be dublikatu:");
            foreach (int elementas in sarasas)
            {
                Console.Write(elementas + " ");
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Laikas pries: " + laikasPries);
            Console.WriteLine("Laikas po: " + laikasPo);
            Console.WriteLine("Trukme: " + (laikasPo - laikasPries));

            Console.ReadLine();

        }
    }
}
