﻿using System;

namespace _4_Masyvas_Per_Kelias_Eilutes
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 2 UZDUOTIS:
             * Atspausdinti po 10 elementu eiluteje
            */

            Console.WriteLine();
            Console.WriteLine("----- 2 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            int counter = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " - ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
                Console.Write(masyvas[i] + " ");
            }

            Console.WriteLine();
            Console.WriteLine();
        }
    }
}
