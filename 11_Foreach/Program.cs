﻿using System;

namespace _11_Foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 10 UZDUOTIS:
             * foreach naudojimas
            */

            Console.WriteLine();
            Console.WriteLine("----- 10 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            Console.WriteLine();

            foreach (int elementas in masyvas)
            {
                //string s = "-" + elementas + 5;
                Console.WriteLine("-" + elementas + 5);
            }

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            //foreach (int elementas in masyvas)
            //{
            //    elementas = randNum.Next(0, 100);
            //}

            int counter = 0;


            //for (int i = 0; i < masyvas.Length; i++)
            //{
            //    Console.Write(masyvas[i] + " ");
            //    if (counter == 9)
            //    {
            //        Console.WriteLine();
            //        counter = 0;
            //    }
            //    else
            //    {
            //        counter++;
            //    }
            //}

            // foreach analogas defaultiniam for'ui 
            foreach (int elementas in masyvas)
            {
                Console.Write(elementas + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }

        }
    }
}
