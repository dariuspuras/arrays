﻿using System;

namespace _8_Elementu_Kiekis
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 6 UZDUOTIS:
             * Kiek masyvo elementu < 50
            */

            Console.WriteLine();
            Console.WriteLine("----- 6 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            int maziauUz50 = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                if (masyvas[i] < 50)
                {
                    maziauUz50++;
                }
            }

            Console.WriteLine();
            Console.WriteLine("Elementu, kurie mazesni uz 50, kiekis: " + maziauUz50);

            /**
             * 7 UZDUOTIS:
             * Skaiciuoti suma elementu, kurie > 50
            */

            Console.WriteLine();
            Console.WriteLine("----- 7 UZDUOTIS -------");
            Console.WriteLine();

            int sumaDaugiauUz50 = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                if (masyvas[i] > 50)
                {
                    sumaDaugiauUz50 = sumaDaugiauUz50 + masyvas[i];
                }
            }

            Console.WriteLine("suma elementu daugiau uz 50: " + sumaDaugiauUz50);

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
