﻿using System;

namespace _1_For_Ciklas
{
    class Program
    {
        static void Main(string[] args)
        {
            int skaicius = 75;

            int counter = 0;

            for (int i = 0; i <= skaicius; i++)
            {
                if (i != 0)
                {
                    Console.WriteLine(i);
                }
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }

            Console.ReadLine();

        }
    }
}
