﻿using System;

namespace _7_Elementu_Suma
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 5 UZDUOTIS:
             * Sumos atspausdinimas
            */

            Console.WriteLine();
            Console.WriteLine("----- 5 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            int suma = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                suma = suma + masyvas[i];
            }

            Console.WriteLine();
            Console.WriteLine("Suma: " + suma);

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
