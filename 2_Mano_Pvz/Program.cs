﻿using System;

namespace _2_Mano_Pvz
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * Komentaro pavyzdys
             * 
             */

            //regiono pradzia
            #region ManoPavyzdziai 

            Console.WriteLine("===================");
            Console.WriteLine("----- MANO PVZ ----");
            Console.WriteLine("===================");
            Console.WriteLine("");

            string[] miestai = new string[] { "Vilnius", "Kaunas", "Siauliai" };

            // Isprintinimas masyvo elementu rankiniu budu
            Console.Write("Rankinis budas: " + miestai[0] + " " + miestai[1] + " " + miestai[2]);
            Console.WriteLine();
            Console.WriteLine();

            // Isprintinimas masyvo elementu su ciklu
            Console.Write("Automatinis budas: ");

            for (int a = 0; a < miestai.Length; a++)
            {
                Console.Write(miestai[a] + " ");
            }

            Console.WriteLine(); // Uzbaigia eilute ir perkelia kursoriu, kadangi po Console.Write
            Console.WriteLine(); // Sukuria tuscia eilute ir perkelia kursoriu, kadangi po Console.WriteLine


            /**
             * Komentaro pavyzdys
             * 
             */

            Console.WriteLine("===================");
            Console.WriteLine("----- MANO PVZ 2 --");
            Console.WriteLine("===================");
            Console.WriteLine("");

            string[] miestai2 = new string[] { "Vilnius", "Kaunas", "Siauliai", "Klaipeda" };

            // Isprintinimas masyvo elementus nuo galo
            Console.Write("Rankinis budas: " + miestai2[3] + " " + miestai2[2] + " " + miestai2[1] + " " + miestai2[0]);
            Console.WriteLine();
            Console.WriteLine();

            // Isprintinimas su ciklu
            Console.Write("Automatinis budas: ");

            for (int a = miestai2.Length - 1; a >= 0; a--)
            {
                Console.Write(miestai2[a] + " ");
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("===================");
            Console.WriteLine("----- MANO PVZ 3 --");
            Console.WriteLine("===================");
            Console.WriteLine("");

            string[] miestai3 = new string[] { "Vilnius", "Kaunas", "Siauliai", "Klaipeda" };


            // Perrikiuoti masyvo elementus nuo galo
            Console.Write("Rankinis budas: " + miestai2[0] + " " + miestai2[1] + " " + miestai2[2] + " " + miestai2[3]);
            Console.WriteLine();
            Console.WriteLine();

            // Isprintinimas su ciklu
            Console.Write("Automatinis budas: ");

            for (int a = miestai2.Length - 1; a >= 0; a--)
            {
                Console.Write(miestai2[a] + " ");
            }

            //regiono pabaiga
            #endregion 

            Console.ReadLine();
        }
    }
}
