﻿using System;

namespace _5_Max_Reiksme
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 3 UZDUOTIS:
             * Maksimalios reiksmes atspausdinimas
            */

            Console.WriteLine();
            Console.WriteLine("----- 3 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            Console.WriteLine();

            int max = 0;

            for (int i = 0; i < masyvas.Length; i++)
            {
                if (masyvas[i] > max)
                {
                    max = masyvas[i];
                }
            }

            Console.WriteLine("MAX: " + max);
            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
