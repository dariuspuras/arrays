﻿using System;

namespace _6_Min_Reiksme
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 4 UZDUOTIS:
             * Minimalios reiksmes atspausdinimas
            */

            Console.WriteLine();
            Console.WriteLine("----- 4 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            Console.WriteLine();

            int min = 20;

            for (int i = 0; i < masyvas.Length; i++)
            {
                if (masyvas[i] < min)
                {
                    min = masyvas[i];
                }
            }

            Console.WriteLine("MIN: " + min);

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
