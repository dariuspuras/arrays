﻿using System;

namespace _3_Random_Masyvas
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 1 UZDUOTIS:
             * Sukurti masyva is random skaiciu nuo 0 iki 100
             * Atspausdinti eiluteje
            */

            Console.WriteLine();
            Console.WriteLine("----- 1 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];

            Console.WriteLine("Masyvo ilgis: " + masyvas.Length);
            Console.WriteLine();

            Console.Write("Masyvas: ");

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
                Console.Write(masyvas[i] + " ");
            }

            Console.ReadLine();
        }
    }
}
