﻿using System;

namespace _10_Elementu_Rikiavimas
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * 9 UZDUOTIS:
             * Surikiuoti masyva didejancia tvarka ir atspausdinti
            */

            Console.WriteLine();
            Console.WriteLine("----- 9 UZDUOTIS -------");
            Console.WriteLine();

            int[] masyvas = new int[20];
            UzpildytiRandomSkaiciais(masyvas);

            // Perrikiavimas
            for (int i = 0; i < masyvas.Length - 1; i++)
            {
                for (int j = i + 1; j < masyvas.Length; j++)
                {
                    if (masyvas[j] < masyvas[i])
                    {
                        int temp = masyvas[j];
                        masyvas[j] = masyvas[i];
                        masyvas[i] = temp;
                    }
                }
            }

            // Trumpesnis budas perrikiuoti masyva
            //Array.Sort(masyvas); 

            // Atspausdinimas i konsole
            Console.WriteLine();
            Console.WriteLine("Perrusiuotas masyvas:");

            int counter = 0; // counteris nuresetinamas i 0

            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");

                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }

            Console.ReadLine();
        }

        static void UzpildytiRandomSkaiciais(int[] masyvas)
        {
            Console.Write("Masyvas: ");
            Console.WriteLine();

            Random randNum = new Random();

            for (int i = 0; i < masyvas.Length; i++)
            {
                masyvas[i] = randNum.Next(0, 100);
            }

            int counter = 0;
            for (int i = 0; i < masyvas.Length; i++)
            {
                Console.Write(masyvas[i] + " ");
                if (counter == 9)
                {
                    Console.WriteLine();
                    counter = 0;
                }
                else
                {
                    counter++;
                }
            }
        }
    }
}
